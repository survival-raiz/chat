package com.gitlab.survivalRaiz.chat;

import api.skwead.commands.CommandManager;
import api.skwead.messages.chat.placeholders.Placeholders;
import com.gitlab.survivalRaiz.chat.channel.ChannelConfig;
import com.gitlab.survivalRaiz.chat.channel.ChannelManager;
import com.gitlab.survivalRaiz.chat.commands.ChatCmd;
import com.gitlab.survivalRaiz.chat.handlers.ChatManager;
import com.gitlab.survivalRaiz.chat.handlers.SpeakEvents;
import com.gitlab.survivalRaiz.clans.Clans;
import com.gitlab.survivalRaiz.core.Core;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Chat extends JavaPlugin {

    private Core core;
    private Clans clans;

    private final ChannelManager channelManager = new ChannelManager();
    private final ChatManager chatManager = new ChatManager(channelManager);

    @Override
    public void onEnable() {
        core = (Core) Bukkit.getPluginManager().getPlugin("Core");
        final Set<CommandSender> console = new HashSet<>(Collections.singleton(getServer().getConsoleSender()));

        if (core == null) return; // TODO: 7/30/20 Mensagem de erro melhor

        clans = (Clans) Bukkit.getPluginManager().getPlugin("Clans");


        final CommandManager commandManager = new CommandManager(this);
        try {
            commandManager.getConfig().generateFrom(new HashSet<>(Collections.singleton(new ChatCmd(this))));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            this.channelManager.load(new ChannelConfig(this));
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Placeholders placeholderManager = new Placeholders(this);

        new SpeakEvents(this);
        Bukkit.getOnlinePlayers().forEach(op -> chatManager.addUser(op.getUniqueId(), "global"));


        core.getMsgHandler().message(console, Message.SERVER_ON);
    }

    @Override
    public void onDisable() {
        final Set<CommandSender> console = new HashSet<>(Collections.singleton(getServer().getConsoleSender()));

        core.getMsgHandler().message(console, Message.SERVER_OFF);
    }

    public ChatManager getChatManager() {
        return this.chatManager;
    }

    public ChannelManager getChannelManager() {
        return channelManager;
    }

    public Core getCore() {
        return core;
    }

    public Clans getClans() {
        return clans;
    }
}
