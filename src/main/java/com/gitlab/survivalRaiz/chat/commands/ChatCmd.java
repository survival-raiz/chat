package com.gitlab.survivalRaiz.chat.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.chat.Chat;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;

public class ChatCmd extends ConfigCommand {
    private final Chat chat;

    public ChatCmd(Chat chat) {
        super("chat", "comando base do chat", "chat <nome do canal>",
                new ArrayList<>(Collections.singleton("c")), "chat-cmd");
        this.chat = chat;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (strings.length == 0)
            throw new SRCommandException(commandSender, Message.SYNTAX, getUsage(), chat.getCore());

        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, chat.getCore());

        final Player p = (Player) commandSender;

        if (this.chat.getChannelManager().getChannel(strings[0]) == null)
            throw new SRCommandException(p, Message.NONEXISTENT_CHANNEL, chat.getCore());

        if (!this.chat.getChannelManager().getChannel(strings[0]).canSpeak(p.getUniqueId()))
            throw new SRCommandException(p, Message.NO_PERMISSIONS, chat.getCore());

        return -1;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        validate(commandSender, s, strings);

        Player p;
        try {
            p = (Player) commandSender;
        } catch (ClassCastException e) {
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, chat.getCore());
        }

        this.chat.getChatManager().addUser(p.getUniqueId(), strings[0]);
        // TODO: 7/31/20 Criar um placeholder para na mensagem mostrar o canal novo.
    }
}
