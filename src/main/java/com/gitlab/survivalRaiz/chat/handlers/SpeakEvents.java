package com.gitlab.survivalRaiz.chat.handlers;

import com.gitlab.survivalRaiz.chat.Chat;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class SpeakEvents implements Listener {

    private final ChatManager cm;

    public SpeakEvents(Chat plugin) {
        this.cm = plugin.getChatManager();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        this.cm.addUser(e.getPlayer().getUniqueId(), "global");
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        e.setCancelled(true);
        this.cm.getActiveChannel(e.getPlayer().getUniqueId()).message(e.getPlayer(), e.getMessage());
    }
}
