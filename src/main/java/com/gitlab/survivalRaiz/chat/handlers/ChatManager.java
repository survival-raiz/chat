package com.gitlab.survivalRaiz.chat.handlers;

import com.gitlab.survivalRaiz.chat.channel.Channel;
import com.gitlab.survivalRaiz.chat.channel.ChannelManager;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ChatManager {
    private final Map<UUID, String> userActiveChat = new HashMap<>();
    private final ChannelManager cm;

    public ChatManager(ChannelManager cm) {
        this.cm = cm;
    }

    public void addUser(UUID uuid, String channelName){
        this.userActiveChat.put(uuid, channelName);
    }

    public Channel getActiveChannel(UUID player){
        final String name = this.userActiveChat.get(player);
        return this.cm.getChannel(name);
    }
}
