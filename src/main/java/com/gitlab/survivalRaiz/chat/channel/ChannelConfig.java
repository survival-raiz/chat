package com.gitlab.survivalRaiz.chat.channel;

import api.skwead.storage.file.yml.YMLConfig;
import com.gitlab.survivalRaiz.chat.Chat;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.*;

/**
 * Manages channel data in a yml config.
 * <p>
 * Recipients field syntax: type:identifier
 * <p>
 * perm:<permission> -> adds all players with said permission to the channel
 * uuid:<uuid> -> add the player with the uuid to the channel
 */
public class ChannelConfig extends YMLConfig {

    private final JavaPlugin plugin;

    public ChannelConfig(JavaPlugin plugin) {
        super("channels", plugin);
        this.plugin = plugin;
    }

    /**
     * Verifies if all channels have the required fields in the config
     * The set input is irrelevant, can be whatever.
     */
    @Override
    public void createConfig(Set<String> set) throws IOException {

        if (super.getConfig().getString("global.format") == null) {
            super.getConfig().createSection("global.format");
            super.getConfig().set("global.format", "null");
            super.getConfig().save(super.getFile());
        }

        if (super.getConfig().getString("global.radius") == null) {
            super.getConfig().createSection("global.radius");
            super.getConfig().set("global.radius", 0);
            super.getConfig().save(super.getFile());
        }

        if (super.getConfig().get("global.recipients") == null) {
            super.getConfig().createSection("global.recipients");
            super.getConfig().set("global.recipients", new String[]{});
            super.getConfig().save(super.getFile());
        }

        if (super.getConfig().get("global.writers") == null) {
            super.getConfig().createSection("global.writers");
            super.getConfig().set("global.writers", new String[]{});
            super.getConfig().save(super.getFile());
        }

//        if (super.getConfig().getConfigurationSection("") == null) return;

        for (String cs : super.getConfig().getConfigurationSection("").getKeys(false)) {
            if (super.getConfig().getString(cs + ".format") == null) {
                super.getConfig().createSection(cs + ".format");
                super.getConfig().set(cs + ".format", "null");
                super.getConfig().save(super.getFile());
            }

            if (super.getConfig().getString(cs + ".radius") == null) {
                super.getConfig().createSection(cs + ".radius");
                super.getConfig().set(cs + ".radius", 0);
                super.getConfig().save(super.getFile());
            }

            if (super.getConfig().get(cs + ".recipients") == null) {
                super.getConfig().createSection(cs + ".recipients");
                super.getConfig().set(cs + ".recipients", new String[]{});
                super.getConfig().save(super.getFile());
            }

            if (super.getConfig().get(cs + ".writers") == null) {
                super.getConfig().createSection(cs + ".writers");
                super.getConfig().set(cs + ".writers", new String[]{});
                super.getConfig().save(super.getFile());
            }
        }
    }

    /**
     * Returns a channel from the data in the config
     *
     * @param name the channel name
     * @return the {@link Channel} object
     */
    public Channel getChannel(String name) {
        if (super.getConfig().get(name) == null) return null;

        final String format = super.getConfig().getString(name + ".format");
        final int radius = super.getConfig().getInt(name + ".radius");

        final List<String> recipients = super.getConfig().getStringList(name + ".recipients");
        final Set<UUID> recUUIDs = new HashSet<>();
        final Set<String> readPerms = new HashSet<>();
        final List<String> senders = super.getConfig().getStringList(name + ".writers");
        final Set<UUID> sendUUIDs = new HashSet<>();
        final Set<String> sendPerms = new HashSet<>();

        recipients.forEach(str -> {
            if (str.startsWith("perm:"))
                readPerms.add(str.substring(5));
            else if (str.startsWith("uuid:"))
                recUUIDs.add(UUID.fromString(str.substring(5)));
        });

        senders.forEach(str -> {
            if (str.startsWith("perm:"))
                sendPerms.add(str.substring(5));
            else if (str.startsWith("uuid:"))
                sendUUIDs.add(UUID.fromString(str.substring(5)));
        });

        final Channel c = new Channel((Chat) this.plugin, name, format, radius);

        recUUIDs.forEach(c::addRecipient);
        readPerms.forEach(c::addReadPerm);
        sendUUIDs.forEach(c::addWriter);
        sendPerms.forEach(c::addSendPerm);

        return c;
    }
}
