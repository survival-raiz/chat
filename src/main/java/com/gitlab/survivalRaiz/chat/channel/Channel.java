package com.gitlab.survivalRaiz.chat.channel;

import com.gitlab.survivalRaiz.chat.Chat;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Channel {
    private final Chat plugin;

    private final String name;
    private final String format;
    private final int radius;
    private final Set<UUID> recipients = new HashSet<>();
    private final Set<String> readPerms = new HashSet<>();
    private final Set<UUID> speakers = new HashSet<>();
    private final Set<String> writePerms = new HashSet<>();

    public Channel(Chat plugin, String name, String format, int radious) {
        this.plugin = plugin;
        this.name = name;
        this.format = format;
        this.radius = radious;
    }

    /**
     * Sends a message with the format in the config for this channel.
     * Takes into account the player's permission to speak.
     *
     * @param sender  the player who sends the message
     * @param message the message sent
     */
    public void message(Player sender, String message) {
        final Set<UUID> recs = new HashSet<>(recipients);

        this.plugin.getServer().getOnlinePlayers().forEach(p -> {
            if (canRead(p.getUniqueId())) recs.add(p.getUniqueId());
        });

        recs.forEach(player -> {
            Player p = Bukkit.getPlayer(player);
            if (p == null) return;

            if (canRead(player) &&
                    (this.radius == 0 || p.getLocation().distance(sender.getLocation()) <= this.radius))
                p.sendMessage(
                        PlaceholderAPI.setPlaceholders(sender,
                                this.format
                                        .replace("%msg%", message))
                                        .replace("%clan%", plugin.getClans().getClanManager().getPlayerClan(sender.getUniqueId()) == null ?
                                                "" :
                                                plugin.getClans().getClanManager().getPlayerClan(sender.getUniqueId()))
                );
        });
    }

    /**
     * Tells if the player can speak in the channel.
     *
     * @param player The player's UUID
     * @return true if the player can speak
     */
    public boolean canSpeak(UUID player) {
        final Player p = Bukkit.getPlayer(player);
        boolean add = true;
        if (p == null) return false;

        for (String perm : this.writePerms) {
            add = add && p.hasPermission(perm);
            if (!add) break;
        }

        return add;
    }

    /**
     * Tells if the player can read messages in the channel
     *
     * @param player The player's UUID
     * @return true if the player can speak
     */
    public boolean canRead(UUID player) {
        final Player p = Bukkit.getPlayer(player);
        boolean add = true;
        if (p == null) return false;

        for (String perm : this.readPerms) {
            add = add && p.hasPermission(perm);
            if (!add) break;
        }

        return add;
    }

    public void addRecipient(UUID p) {
        this.recipients.add(p);
    }

    public void addReadPerm(String perm) {
        this.readPerms.add(perm);
    }

    public void addWriter(UUID p) {
        this.speakers.add(p);
    }

    public void addSendPerm(String perm) {
        this.writePerms.add(perm);
    }

    public String getName() {
        return name;
    }
}
