package com.gitlab.survivalRaiz.chat.channel;

import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class ChannelManager {
    private final Set<Channel> channels = new HashSet<>();

    public void load(ChannelConfig conf) throws IOException {
        conf.createConfig(null);

        final Set<String> chans =
                Objects.requireNonNull(conf.getConfig().getConfigurationSection("")).getKeys(false);

        chans.forEach(c -> this.channels.add(conf.getChannel(c)));
    }

    public Channel getChannel(String name){
        for (Channel channel : this.channels) {
            if (channel.getName().equals(name))
                return channel;
        }

        return null;
    }
}
